const $board = $('#board');

//Función para generar el tablero de juego
function createBoard (rows, columns){
    $board.empty(); //Para limpiar todos los elementos que haya en el board
    for (let i = 0; i < rows; i++){ //Primer for para loopear en filas
        const $row = $('<div>').addClass('row');
        for(let j = 0; j < columns; j++){
            const $column = $('<div>')
            .addClass('column hidden'); //Creamos una clase 'hidden' para hacer como 2 'states' uno donde saldra el número y otro en blanco
            if(Math.random() < 0.1) {
                $column.addClass('mine');
            }
            $row.append($column); //Aquí hacemos lo mismo, introducimos las columnas en las filas, así se genera el tablero
        }
        $board.append($row); //Append es para introducir el div dentro de otro div
    }
}

createBoard(9, 9);

function restart(){
    createBoard(9, 9);
}

function gameOver(win) { //Función para mensaje
    let message = null;
    if (win) {
        message = 'Has ganado!'
    }else{
        message = 'Has perdido :('
    }

    alert(message);
    restart();
}

$board.on('click', '.column.hidden', function() {
    const $cell = $(this);
    if ($cell.hasClass('mine')) { //Seleccionamos los divs con clase 'mine'
        gameOver(false);
    }
})